'use strict'

/**
 *   Toggle the visibilty of a form group
 *  
 *   @param      {string}    form_id  The form identifier
 *   @param      {boolean}   show     Whether to show or hide
 */
function toggle_visibilty_of_form_group(form_id, show) {
  let form_element = $(form_id);
  let parent = form_element.parent();

  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/**
 *  Toggle the visibilty of the SLURM queue fields
 *  
 *  interactive_mode: hidden
 *  interactive_mode_gpu: hidden
 *  batch_mode: visible
 */
function toggle_slurm_queues_field_visibility() {
  let type_ofuse = $("#batch_connect_session_context_type_ofuse");

  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_job_name',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_slurm_partition',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_slurm_account',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_qos_name',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_num_nodes',
    type_ofuse.val() === 'batch'
  );
/*  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_num_cores',
    type_ofuse.val() === 'batch'
  );
*/
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_gres_value',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
     '#batch_connect_session_context_account_ood',
    type_ofuse.val() === 'interactive'	  
  );	  
}

/*
 *  Toggle the visibility of the SLURM account field 
 */
function toggle_slurm_account_field_visibility() {
  let accounting_partitions = [
    'dirac1', 'cf1', 'cf1-hp', 'cm1', 'cm2', 'csd_lr6_192', 'csd_lr6_96', 'csd_`lr6_share', 'es1', 'lr3', 'lr4', 'lr5', 'lr6', 'lr_bigmem'
  ];
  
  const show = accounting_partitions.includes(event.target.value);
  let form_element = $("#batch_connect_session_context_slurm_account");
  let parent = form_element.parent();
  
  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/*
 *  Toggle the visibility of the SLURM account field for interactive OOD
 */
function toggle_slurm_account_field_visibility() {
  let accounting_partitions = ['ood_inter'];

  const show = accounting_partitions.includes(event.target.value);
  let form_element = $("#batch_connect_session_context_slurm_account_ood");
  let parent = form_element.parent();

  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/*
 *  Toggle the visibility of the SLURM QoS field 
 */
function toggle_slurm_qos_field_visibility() {
  let accounting_partitions = [
    'dirac1', 'cf1', 'cf1-hp', 'cm1', 'cm2', 'csd_lr6_192', 'csd_lr6_96', 'csd_`lr6_share', 'es1', 'lr3', 'lr4', 'lr5', 'lr6', 'lr_bigmem'
  ];
  const show = accounting_partitions.includes(event.target.value);
  let form_element = $("#batch_connect_session_context_qos_name");
  let parent = form_element.parent();
  
  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/*
 *  Toggle the visibility of the CPU cores field 
 */
function toggle_cpu_cores_field_visibility() {
  let per_core_partitions = [
    'alice', 'alsacc', 'etna-shared', 'etna-gpu', 'jbei1', 'jgi', 'csd_lr6_share', 'es1', 'mhg', 'cm1', 'xmas'
  ];

  const show = per_core_partitions.includes(event.target.value);
  let form_element = $("#batch_connect_session_context_num_cores");
  let parent = form_element.parent();
  
  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/**
 *  Toggle the visibility of the GRES Value field
 */
function toggle_gres_value_field_visibility() {
  let gpu_partitions = [
    'es1', 'etna_gpu'
  ];

  const show = gpu_partitions.includes(event.target.value);
  let form_element = $("#batch_connect_session_context_gres_value");
  let parent = form_element.parent();

  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/**
 * Sets the change handler for the batch type of usage select.
 */
function set_type_ofuse_change_handler() {
  let type_ofuse = $("#batch_connect_session_context_type_ofuse");
  type_ofuse.change(toggle_slurm_queues_field_visibility);
}

/**
 * Sets the change handler for the slurm partition select.
 */
function set_slurm_partition_change_handler() {
  let slurm_partition = $("#batch_connect_session_context_slurm_partition");
  slurm_partition.change(function(event){
    toggle_slurm_account_field_visibility(event);
    toggle_slurm_qos_field_visibility(event);
    toggle_cpu_cores_field_visibility(event);
    toggle_gres_value_field_visibility(event);
  });
}


/**
 *  Install event handlers
 */
$(document).ready(function() {
  // Ensure that fields are shown or hidden based on what was set in the last session
  toggle_slurm_queues_field_visibility();
  
  set_type_ofuse_change_handler();
  set_slurm_partition_change_handler();
});




